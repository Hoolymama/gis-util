const DEG_TO_RAD =  Math.PI / 180.0
const EARTH_RADIUS = 6371000 /* meters  */
const THREE_PI = Math.PI*3
const TWO_PI = Math.PI*2


function define(name, value) {
	Object.defineProperty(exports, name, {
		value:      value,
		enumerable: true
	});
}
define("DEG_TO_RAD", DEG_TO_RAD);
define("EARTH_RADIUS",EARTH_RADIUS);
define("THREE_PI", THREE_PI);
define("TWO_PI", TWO_PI);

function isFloat(n)  {return !isNaN(parseFloat(n)) && isFinite(n);}

function recursiveConvert(input, callback){
	if (input instanceof Array) {
		return input.map((el) => recursiveConvert(el, callback))
	}
	if  (input instanceof Object) {
		input = JSON.parse(JSON.stringify(input))
		for (let key in input) {
			if( input.hasOwnProperty(key) ) {
				input[key] = recursiveConvert(input[key], callback) 
			} 
		}
		return input
	}
	if (isFloat(input)) { 
		return callback(input) 
	} else {
		return(input)
	}

}


function isGeoJson(coord){
	return (coord.hasOwnProperty("coordinates")) 
}

function isLatLon(coord){
	return (coord.hasOwnProperty("longitude")) 
}

function randomRange(low, high){
	return (Math.random()*(high-low)) + low
}

exports.toGeoJson = (latlon) => {
	return {type: "Point", coordinates: [latlon.longitude, latlon.latitude]}
}

exports.toLatLon = (geoJson) => {
	return {longitude: geoJson.coordinates[0], latitude:geoJson.coordinates[1]}
}

exports.toRadians = (input) =>
recursiveConvert(input, (val) => val * DEG_TO_RAD)

exports.toDegrees = (input) => 
recursiveConvert(input, (val) => val / DEG_TO_RAD)

exports.randomGeoJson = () => exports.toGeoJson(exports.randomLatLon())

exports.randomLatLon = () =>   {
	return {
		latitude: randomRange(-90, 90),
		longitude: randomRange(-180, 180),
		time: new Date()
	}
}

exports.pointAtDistance = (inputCoords, distance, bearing) => {
	let result = {}
	const isGJ = isGeoJson(inputCoords)
	let coords = exports.toRadians(inputCoords)
	if (isGJ) {
		coords =  exports.toLatLon(coords)
	}
	const sinLat = Math.sin(coords.latitude)
	const cosLat = Math.cos(coords.latitude)
	const theta = distance/EARTH_RADIUS
	const sinBearing = Math.sin(bearing)
	const cosBearing = Math.cos(bearing)
	const sinTheta = Math.sin(theta)
	const cosTheta = Math.cos(theta)
	result.latitude = Math.asin(sinLat*cosTheta+cosLat*sinTheta*cosBearing)
	result.longitude = coords.longitude + 
	Math.atan2( sinBearing*sinTheta*cosLat, cosTheta-sinLat*Math.sin(result.latitude )
		)
	result.longitude = ((result.longitude+THREE_PI)%TWO_PI)-Math.PI
	if (isGJ) {
		result=exports.toGeoJson(result)
	}
	return exports.toDegrees(result)
}

exports.randomPointInRing = (coord, minDistance, maxDistance) => {
	if (minDistance>maxDistance){let tmp = maxDistance;maxDistance=minDistance;minDistance=tmp}
	const minNorm =  minDistance/maxDistance
	const rnd = randomRange(minNorm, 1)
	const randomDist = Math.sqrt(rnd) * maxDistance
	const bearing = Math.random() * 360
	return exports.pointAtDistance(coord, randomDist, bearing)
}

/* haversine */
exports.distanceBetween = (start, end) => {
	let startPoint = exports.toRadians(start)
	let endPoint = exports.toRadians(end)
	if (isGeoJson(startPoint)) {
		startPoint= exports.toLatLon(startPoint)
	}
	if (isGeoJson(endPoint)) {
		endPoint= exports.toLatLon(endPoint)
	}
	const delta = {
		latitude:  Math.sin((endPoint.latitude -startPoint.latitude)/2),
		longitude: Math.sin((endPoint.longitude -startPoint.longitude)/2)
	}

	const A = delta.latitude * delta.latitude  + 
	delta.longitude *  delta.longitude * Math.cos(startPoint.latitude) * Math.cos(endPoint.latitude)

	return  EARTH_RADIUS*2 * Math.atan2(Math.sqrt(A), Math.sqrt(1-A))
}

exports.pointsAreClose = (start, end, distance) => {

	const thresholdRadians = distance/EARTH_RADIUS
	const isG = (isGeoJson(start) && isGeoJson(end) )
	let startPoint = exports.toRadians(start)
	let endPoint = exports.toRadians(end)
	if (isG) {
		startPoint =  exports.toLatLon(startPoint)
		endPoint =  exports.toLatLon(endPoint)
	}

	if (Math.abs(startPoint.latitude - endPoint.latitude) > thresholdRadians) {
		return false
	}
	const latMult =  Math.max( Math.cos(startPoint.latitude), Math.cos(endPoint.latitude))
	let diff =  Math.abs(startPoint.longitude - endPoint.longitude)
  diff = (diff >  Math.PI) ? (TWO_PI - diff) : diff

	if ( (diff* latMult) > thresholdRadians) {
		return false
	}

	if (exports.distanceBetween(start, end) > distance) {
		return false
	}
	return true
}

exports.bearing = (start, end) => {
	let startPoint = exports.toRadians(start)
	let endPoint = exports.toRadians(end)

	if (isGeoJson(startPoint)) {
		startPoint = exports.toLatLon(startPoint)
	}
	if (isGeoJson(endPoint)) {
		endPoint = exports.toLatLon(endPoint)
	}

	const deltaLon = endPoint.longitude-startPoint.longitude
	const cosEndLat = Math.cos(endPoint.latitude)
	const sinEndLat = Math.sin(endPoint.latitude)
	const sinStartLat = Math.sin(startPoint.latitude)
	const cosStartLat = Math.cos(startPoint.latitude)
	const sinDeltaLon = Math.sin(deltaLon)
	const cosDeltaLon = Math.cos(deltaLon)

	const y = sinDeltaLon * cosEndLat;
	const x = cosStartLat*sinEndLat - sinStartLat*cosEndLat*cosDeltaLon;

	const bearing = (Math.atan2(y, x)+ TWO_PI) % TWO_PI

	return exports.toDegrees(bearing)
}

