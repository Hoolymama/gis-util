const expect = require("chai").expect
const  Gis = require("../src/index.js")
const _ = require("underscore")

describe("Geo Lib", () => {

	const epsilon = 1.0e-6

	let randomPt, randomDist
	beforeEach(function (){
		randomPt = {
			latitude:(Math.random()*180)-90, 
			longitude:(Math.random()*360)-180
		}
		randomDist = Math.random()*50000
	})



	it("should measure distance between two latLon positions", function() {
		const diff = Gis.distanceBetween({latitude: 0 , longitude: 0},randomPt )
		expect(diff).to.be.above(1);
	})


	it("should convert latLon to geoJson", function() {
		const gj = Gis.toGeoJson(randomPt)
		expect(gj).to.have.property("coordinates") 
	})

	it("should convert geoJson to latLon", function() {
		const gj = Gis.toGeoJson(randomPt)
		expect(gj).to.have.property("coordinates") 
	})

	it("toGeoJson should provide valid geoJson", function() {
		const gj = Gis.toGeoJson(randomPt)
		expect(gj).to.have.property("type", "Point") 
	})
	
	it("pointAtDistance should provide valid geoJson", function() {
		const gj = Gis.pointAtDistance( Gis.toGeoJson(randomPt), randomDist, 45)
		expect(gj).to.have.property("type", "Point") 
	})

	it("should create a point on circle", function() {
		const pt = Gis.randomPointInRing(randomPt, randomDist, randomDist)
		const dist = Gis.distanceBetween(pt, randomPt)
		expect(randomDist+epsilon).to.be.above(dist)
	})


	it("should create a GeoJson point on circle when inputs are geoJson", function() {
		const gRandomPoint = Gis.toGeoJson(randomPt)
		const gpt = Gis.randomPointInRing(gRandomPoint, randomDist, randomDist)
 		expect(gpt).to.have.property("coordinates") 
		const dist = Gis.distanceBetween(gpt,  gRandomPoint)
		expect(randomDist+epsilon).to.be.above(dist)
	})


	it("should create a point at distance", function() {
		const pt = Gis.pointAtDistance(randomPt, randomDist, 45)
		const dist = Gis.distanceBetween(pt, randomPt)
		const diff = Math.abs(randomDist-dist)
		const errFactor = Math.abs(diff / randomDist)
		expect(errFactor).to.be.below(epsilon)
	})

	it("should create many points in ring", function() {
		let [min,max] = [Math.random()*50000 , Math.random()*50000 ]
		if (min>max){
			let tmp = max
			max=min
			min=tmp
		}
		let errorCount = 0
		_(100).times((n) => {
			const pt = Gis.randomPointInRing(randomPt, min, max)
			const dist = Gis.distanceBetween(pt, randomPt)
			if (dist < (min-epsilon) || dist >( max+epsilon))
			{ 
				errorCount++ 
			}
		})
		expect(errorCount).to.equal(0)
	})

  it("geoJson results should have a type Point", function() {
		const pt = Gis.pointAtDistance(randomPt, randomDist, 45)
	})


  it("should know if points are close", function() {
		let errorCount = 0
		_(1000).times((n) => {
			const pt = Gis.randomPointInRing(randomPt, 0, randomDist)
			if (! Gis.pointsAreClose(pt, randomPt, randomDist)) {
				errorCount++
			}
		})
		expect(errorCount).to.equal(0)
	})

  
  it("should know if points are far", function() {
		let errorCount = 0
		_(1000).times((n) => {	
			const pt = Gis.randomPointInRing(randomPt, randomDist, randomDist*2)
			if (Gis.pointsAreClose(pt, randomPt, randomDist)) {
				errorCount++
			}
		})
		expect(errorCount).to.equal(0)
	})

})
