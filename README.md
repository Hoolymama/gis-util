GIS Utilities
=========

Work with location data on planet earth

## Installation

  `npm install @hoolymama/gis-util`

## Usage

    var Gis = require("gis")
    Gis.pointAtDistance(coords, distance, bearing)

Create a point 35km east of Bromley, South London

    var bromley = {
      "latitude":51.415805,
      "longitude":   0.0
    }
    var rochester = Gis.pointAtDistance(bromley, 35000, 90)

Measure the distance between Bromley and Rochester using Haversine formula
 
    var dist = Gis.distanceBetween(bromley, rochester)

Generate a point in a random direction between 1km and 2km from Bromley

    var somewhere = Gis.randomPointInRing(bromley, 1000, 2000)

Generate a point in a random direction within 2km of Bromley

    var somewhere = Gis.randomPointInRing(bromley, 0, 2000)

Generate a point in a random direction exactly 2km from Bromley

    var somewhere = Gis.randomPointInRing(bromley, 2000, 2000)

Find the initial bearing when heading to Rochester from Bromley.

    var bearing =  Gis.bearing(bromley, rochester)

You can also pass GeoJson points to:

    bearing()
    pointAtDistance()
    randomPointInRing()
    distanceBetween()

    var bromley = {
      "type": "Point",
      "coordinates": [0.0, 51.415805]
    }

    // pointAtDistance() will return a geoJson point if the input was geoJson
    var rochester = Gis.pointAtDistance(bromley, 35000, 90)

    // same goes for
    randomPointInRing()

## Tests

  `npm test`

## Contributing

You are sure to have better code style than me, so go for it.